import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
// url  = 'http://localhost:4200/assets/peliculas.json'; para bbdd local

urlMoviedb = 'https://api.themoviedb.org/3';
apiKey = '909ca005a8e79a700f0e74ddca2ab3e7';

  constructor(private http:HttpClient) { }

  getMovies(){
  const moviedbUrl = `${this.urlMoviedb}/discover/movie?sort_by=popularity.desc&api_key=${this.apiKey}`;
  return this.http.get<any[]>(moviedbUrl);
  //return this.http.get<any[]>(this.url);
  }


  getMoviesbyId(movie_id){
 const moviedbUrl = `${this.urlMoviedb}/movie/${movie_id}?api_key=${this.apiKey}`;
 return this.http.get<any[]>(moviedbUrl);
   }

   iraPeli(nombre){
    const moviedbUrl = `${this.urlMoviedb}/search/movie?query=${nombre}&api_key=${this.apiKey}`;
    return this.http.get<any[]>(moviedbUrl);
     }


     


//   getMoviesbyId(id: number){
//     for(let x of this.movies){
//       if(x.id == id){
//         return x;
//       }
//     }
// }

// iraPeli(nombre:string): Movie[]{
//   var encontradas: Movie[] = [];
//   nombre.trim().toLowerCase();
//   for(let x of this.movies){
//     let min = x.nombre.toLowerCase();
//     if(min.indexOf(nombre) !== -1){
//       encontradas.push(x);
//       console.log(encontradas);
//     }
//   }
//   return encontradas;
// }



}
export interface Movie{
  id: number;
  slug: string;
  nombre: string;
  bio: string;
  img: string;
  aparicion: string;
};



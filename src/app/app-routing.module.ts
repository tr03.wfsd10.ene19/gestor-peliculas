import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from 'src/app/header/header.component';
import { LoginComponent } from 'src/app/login/login.component';
import { MenuComponent } from 'src/app/menu/menu.component';
import { LogoComponent } from 'src/app/logo/logo.component';
import { SideNavigationComponent } from 'src/app/side-navigation/side-navigation.component';
import { SearchComponent } from 'src/app/search/search.component';
import { DetallepeliculaComponent } from 'src/app/detallepelicula/detallepelicula.component';
import { EncontradoComponent } from 'src/app/encontrado/encontrado.component';

const routes: Routes = [
  {path:'header', component: HeaderComponent},
  {path:'login', component: LoginComponent},
  {path:'logo', component: LogoComponent},
  {path:'menu', component: MenuComponent},
  {path:'search', component: SearchComponent},
  {path:'side-navigation', component: SideNavigationComponent},
  {path:'detallepelicula/:id', component:DetallepeliculaComponent},
  {path:'encontrado/:nombre', component:EncontradoComponent},
  {path:'**', component:HeaderComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

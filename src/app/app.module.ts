import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SideNavigationComponent } from './side-navigation/side-navigation.component';
import { HeaderComponent } from './header/header.component';
import { LogoComponent } from './logo/logo.component';
import { SearchComponent } from './search/search.component';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { MovieCardComponent } from './movie-card/movie-card.component';
import { GeneralpipePipe } from './generalpipe.pipe';
import { DetallepeliculaComponent } from './detallepelicula/detallepelicula.component';
import { EncontradoComponent } from './encontrado/encontrado.component';
import { ImagePipe } from './image.pipe';
import { MyPipePipe } from './my-pipe.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SideNavigationComponent,
    HeaderComponent,
    LogoComponent,
    SearchComponent,
    LoginComponent,
    MenuComponent,
    MovieCardComponent,
    GeneralpipePipe,
    DetallepeliculaComponent,
    EncontradoComponent,
    ImagePipe,
    MyPipePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

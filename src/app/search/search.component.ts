import { Component, OnInit } from '@angular/core';
import { MoviesService, Movie } from '../servicios/movies.service';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser/src/browser/title';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  peliculas: Movie[] = [];
  constructor(private _moviesService: MoviesService,
    private router: Router) { }

  ngOnInit() {
    // this.peliculas = this._moviesService.getMovies();

  }

  buscarPeli(nombre: string){
    this.router.navigate(['/encontrado', nombre]);
    
  }


}

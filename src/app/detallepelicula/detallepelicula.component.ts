import { Component, OnInit } from '@angular/core';
import { MoviesService, Movie } from '../servicios/movies.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-detallepelicula',
  templateUrl: './detallepelicula.component.html',
  styleUrls: ['./detallepelicula.component.css']
})
export class DetallepeliculaComponent implements OnInit {

  movie: any  ;
  
  constructor(private _moviesService: MoviesService, 
              private activatedRoute: ActivatedRoute) { 

                this.activatedRoute.params.subscribe(params =>{
                  this._moviesService.getMoviesbyId(params['id']).subscribe(lista=>this.movie = lista);
                });
                
  }

  ngOnInit() {
    
  }

}

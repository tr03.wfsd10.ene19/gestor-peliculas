import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'image'
})
export class ImagePipe implements PipeTransform {
  imagePathUrl = 'https://image.tmdb.org/t/p/w500';

  transform(value: string): string {
    return this.imagePathUrl + value;
  }

}


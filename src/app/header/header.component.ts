import { Component, OnInit } from '@angular/core';
import { MoviesService, Movie } from '../servicios/movies.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
peliculas: Movie[] = [];

  constructor(private _moviesService: MoviesService,
              private router: Router
              ) { 
                this._moviesService.getMovies().subscribe(lista=>{
                  this.peliculas = lista['results'];
                }); 
  }

  ngOnInit() {
 }

}






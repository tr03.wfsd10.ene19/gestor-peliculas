import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myPipe'
})
export class MyPipePipe implements PipeTransform {

  transform(value: string): any {
  	value = value.substr(0,20) + "...";
    return value;
  }

}

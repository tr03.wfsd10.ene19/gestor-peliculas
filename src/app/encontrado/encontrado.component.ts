import { Component, OnInit } from '@angular/core';
import { MoviesService, Movie } from '../servicios/movies.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-encontrado',
  templateUrl: './encontrado.component.html',
  styleUrls: ['./encontrado.component.css']
})
export class EncontradoComponent implements OnInit {

  movie: Movie[] = [];

  constructor(private _moviesService: MoviesService, 
              private activatedRoute: ActivatedRoute) { 

          this.activatedRoute.params.subscribe( params =>{
            console.log(params['nombre']);
          // this.movie = this._moviesService.iraPeli(params['nombre']);
          this._moviesService.iraPeli(params['nombre']).subscribe(lista=>this.movie = lista["results"]);
            
          });
                
  }

  ngOnInit() {
  }

  
}

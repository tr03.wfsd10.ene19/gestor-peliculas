import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'generalpipe'
})
export class GeneralpipePipe implements PipeTransform {
  transform(value: string): any {
    value = value.toLocaleLowerCase();

    const palabras: string[] = value.split(' ');
    const cadPalabras : string[] = [];

    for (let palabra of palabras){
      palabra = palabra[0].toUpperCase() + palabra.substr(1);
      cadPalabras.push(palabra);
    }
    return cadPalabras.join(' ');
  }
}

import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.css']
})
export class MovieCardComponent implements OnInit {
  constructor(private router: Router){}

  texto : string = "hola";

  @Input() movie:any;
  isVisible:boolean = false;
  

  ngOnInit() {

  }

  irAdetalle(id:number){
    this.router.navigate(['/detallepelicula', id]);
  }

}
